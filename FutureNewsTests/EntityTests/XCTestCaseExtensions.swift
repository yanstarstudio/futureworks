//
//  XCTestCaseExtensions.swift
//  FutureNews
//
//  Created by Haiyan Ma on 20/08/2017.
//  Copyright © 2017 Haiyan Ma. All rights reserved.
//

import XCTest
import Foundation
@testable import FutureNews

extension XCTestCase {
    
    func loadJSONFile(of name: String) -> JSONDictionary? {
        let bundle = Bundle(for: type(of: self))
        
        guard let url = bundle.url(forResource: name, withExtension: "json") else {
            XCTFail("Missing file: \(name).json")
            return nil
        }
        
        guard let data = try? Data(contentsOf: url) else {
            XCTFail("No data from \(url)")
            return nil
        }
        
        guard let json = try? JSONSerialization.jsonObject(with: data, options: []) else {
            XCTFail("json deserialization failed")
            return nil
        }
        return json as? [String: AnyObject]
    }
    
    func createDataFromJSONFile(of name: String) -> Data? {
        let bundle = Bundle(for: type(of: self))
        
        guard let url = bundle.url(forResource: name, withExtension: "json") else {
            XCTFail("Missing file: \(name).json")
            return nil
        }
        return try? Data(contentsOf: url)
    }
}
