//
//  NewsDetailTests.swift
//  FutureNews
//
//  Created by Haiyan Ma on 20/08/2017.
//  Copyright © 2017 Haiyan Ma. All rights reserved.
//

import XCTest
@testable import FutureNews

class NewsDetailTests: XCTestCase {

    override func setUp() {
        super.setUp()
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    func test_Init_TakesNewsProperties() {
        _ = NewsDetail(title: "title", source: "source", dateString: "date", content: "content", imageUrlString: "imageString")
    }
    
    func test_WhenGivenNews_SetsNewsProperties() {
        let newsDetail = NewsDetail(title: "title", source: "source", dateString: "date", content: "content", imageUrlString: "imageString")
        XCTAssertEqual(newsDetail.title, "title")
        XCTAssertEqual(newsDetail.source, "source")
        XCTAssertEqual(newsDetail.dateString, "date")
        XCTAssertEqual(newsDetail.content, "content")
        XCTAssertEqual(newsDetail.imageUrlString, "imageString")
    }
    
    func test_WhenGivenCorrectJSON_NewsCorrectlyInited() {
        guard let json = loadJSONFile(of: "newsDetail") else {
            XCTFail()
            return
        }

        guard let newsDetailObject = NewsDetail(dict: json) else {
            XCTFail("News Detail object initiation results nil")
            return
        }

        XCTAssertEqual("title1", newsDetailObject.title)
        XCTAssertEqual("https://cdn.macrumors.com/article-new/2017/07/DEOsfxlXcAA7RYl.jpg-large-800x600.jpeg", newsDetailObject.imageUrlString)
        XCTAssertEqual("macrumors", newsDetailObject.source)
        XCTAssertEqual("content1", newsDetailObject.content)
        XCTAssertEqual("11/12/2016", newsDetailObject.dateString)
    }
}
