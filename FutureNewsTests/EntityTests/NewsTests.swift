//
//  NewsTests.swift
//  FutureNewsTests
//
//  Created by Haiyan Ma on 19/08/2017.
//  Copyright © 2017 Haiyan Ma. All rights reserved.
//

import XCTest
@testable import FutureNews

class NewsTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    func test_Init_TakesNewsProperties() {
        _ = News(title: "title", summary: "summary", iconUrlString: "iconUrlString", dateString: "date", id: 100)
    }
    
    func test_WhenGivenNews_SetsNewsProperties() {
        let news = News(title: "title", summary: "summary", iconUrlString: "iconUrlString", dateString: "date", id: 100)
        XCTAssertEqual(news.title, "title")
        XCTAssertEqual(news.summary, "summary")
        XCTAssertEqual(news.iconUrlString, "iconUrlString")
        XCTAssertEqual(news.dateString, "date")
        XCTAssertEqual(news.id, 100)
    }
    
    func test_WhenGivenCorrectJSON_NewsCorrectlyInited() {
        guard let json = loadJSONFile(of: "newsFeed") else {
            XCTFail()
            return
        }
        guard let allNews = json["articles"] as? [JSONDictionary], let news = allNews.first else {
            XCTFail("newsFeed key wrong, or newsfeed empty")
            return
        }
        
        guard let newsObject = News(dict: news) else {
            XCTFail("News object initiation results NewsObject nil")
            return
        }
        
        XCTAssertEqual("11/12/2016", newsObject.dateString)
        XCTAssertEqual(100, newsObject.id)
        XCTAssertEqual("https://cdn.macrumors.com/article-new/2017/07/DEOsfxlXcAA7RYl.jpg-large-800x600.jpeg", newsObject.iconUrlString)
        XCTAssertEqual("title1", newsObject.title)
        XCTAssertEqual("summary1", newsObject.summary)
    }    
}
