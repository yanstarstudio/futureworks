//
//  NewsDetailTests.swift
//  FutureNews
//
//  Created by Haiyan Ma on 20/08/2017.
//  Copyright © 2017 Haiyan Ma. All rights reserved.
//

import XCTest
@testable import FutureNews

class NewsDetailControllerTests: XCTestCase {
    
    class MockNewsDetailController: NewsDetailController {
        var timesGetNewsDetailCalled: Int = 0
        override func getDetail() {
            super.getDetail()
            timesGetNewsDetailCalled += 1
        }
    }
    
    var sut: MockNewsDetailController!
    let dataProvider: NewsDetailProvider = NewsDetailProvider(newsId: 100)
    
    override func setUp() {
        super.setUp()
        sut =  MockNewsDetailController(with: dataProvider)
        _ = sut.view // to trigger the call to viewDidLoad
    }

    override func tearDown() {
        super.tearDown()
    }

    func test_NewsDetailDataProviderNotNil() {
        XCTAssertNotNil(sut.newsDetailProvider)
    }
    
    func test_LoadingView_SetsCollectionViewDataSource() {
        XCTAssertTrue(sut.collectionView?.dataSource is NewsDetailProvider)
    }
    
    func test_LoadingView_SetsCollectionViewDelegate() {
        XCTAssertTrue(sut.collectionView?.delegate is NewsDetailProvider)
    }
    
    func test_FetchNewsDetailCalledOnce() {
        XCTAssertEqual(sut.timesGetNewsDetailCalled, 1)
    }
}
