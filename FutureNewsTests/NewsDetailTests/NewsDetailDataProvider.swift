//
//  NewsDetailDataProvider.swift
//  FutureNews
//
//  Created by Haiyan Ma on 21/08/2017.
//  Copyright © 2017 Haiyan Ma. All rights reserved.
//

import XCTest
@testable import FutureNews

class NewsDetailDataProviderTests: XCTestCase {
    
    var sut: NewsDetailProvider!
    var collectionView: UICollectionView!
    var controller: NewsDetailController!
    
    let newsDetail = NewsDetail(title: "title", source: "source", dateString: "date", content: "content", imageUrlString: "imageString")
    
    override func setUp() {
        super.setUp()
        
        sut = NewsDetailProvider(newsId: 100)
        sut.newsDetail = newsDetail
        controller = NewsDetailController(with: sut)
        _ = controller.view
        
        collectionView = controller.collectionView
        collectionView.dataSource = sut
        collectionView.delegate = sut
        
        collectionView.register(ContentCell.self, forCellWithReuseIdentifier: AppConfig.cellId)
    }
    
    func test_NewsIdSetCorrectly() {
        XCTAssertEqual(sut.newsId, 100)
    }
    
    func test_NumberOfSections_IsOne() {
        XCTAssertEqual(collectionView.numberOfSections, 1)
    }
    
    func test_NumberOfItemsInSection0() {
        XCTAssertEqual(collectionView.numberOfItems(inSection: 0), 1)
    }
    
    func test_CellForItem_ReturnsContentCell() {
        let datasource = sut as UICollectionViewDataSource
        let cell = datasource.collectionView(collectionView, cellForItemAt: IndexPath(item: 0, section: 0))
        XCTAssertTrue(cell is ContentCell)
    }
}
