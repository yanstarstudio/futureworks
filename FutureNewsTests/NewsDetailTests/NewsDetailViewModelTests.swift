//
//  NewsDetailViewModelTests.swift
//  FutureNews
//
//  Created by Haiyan Ma on 21/08/2017.
//  Copyright © 2017 Haiyan Ma. All rights reserved.
//

import XCTest
@testable import FutureNews

class NewsDetailViewModelTests: XCTestCase {
    
    struct TestDisplayable: NewsDetailDisplayable {
        let title = "News Title"
        let source = "bbc"
        let dateString = "02/05/2017"
        let imageUrlString = "imageUrlString"
        let content = "I am the content of this test"
    }

    override func setUp() {
        super.setUp()
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    func test_NewsFeedDisplayable() {
        let displayable = TestDisplayable()
        XCTAssertEqual("News Title" + "\n" + "bbc - 02.05.2017", displayable.attributedTitleString.string)
        XCTAssertEqual("I am the content of this test", displayable.content)
        XCTAssertEqual("imageUrlString", displayable.imageUrlString)
    }
}
