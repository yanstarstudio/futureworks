//
//  NewsClientsTests.swift
//  FutureNews
//
//  Created by Haiyan Ma on 20/08/2017.
//  Copyright © 2017 Haiyan Ma. All rights reserved.
//

import Foundation
import XCTest
@testable import FutureNews

extension NetworkClientTests {
    // --------------------------------
    // MARK:- Test URLs
    // --------------------------------
    func test_GetNewsFeedList_UsesExpectedURL() {
        let data = "".data(using: .utf8)
        let engine = NetworkEngineMock(data: data, urlResponse: nil, error: nil)
        let client = NetworkClient(engine: engine)
        let completion = { (_ news: [News], _ error: Error?) in }
        client.getNewsFeed(with: Constants.Methods.newsfeed, completion: completion)
        let urlComponents = getUrlComponents(for: engine)
        XCTAssertEqual(urlComponents?.path, Constants.ApiPath +  Constants.Methods.newsfeed)
    }
    
    func test_GetNewsDetail_UsesExpectedURL() {
        let data = "".data(using: .utf8)
        let engine = NetworkEngineMock(data: data, urlResponse: nil, error: nil)
        let client = NetworkClient(engine: engine)
        let completion = { (_ newsDetail: NewsDetail?, _ error: Error?) in }
        
        client.getNewsDetail(newId: 100, completion: completion)
        let urlComponents = getUrlComponents(for: engine)
        XCTAssertEqual(urlComponents?.path, Constants.ApiPath + "/100.json" )
    }
    
    // ------------------------------------
    // MARK:- Test Model Creation
    // ------------------------------------
    func test_GetNews_CreateNewsObject() {
        guard let data = createDataFromJSONFile(of: "newsFeed") else {
            XCTFail("no data from json file newsFeed.json")
            return
        }
        let engine = NetworkEngineMock(data: data, urlResponse: nil, error: nil)
        let client = NetworkClient(engine: engine)
        client.getNewsFeed(with: Constants.Methods.newsfeed) { (news, error) in
            XCTAssertTrue(news.count == 2)
            XCTAssertNil(error)
        }
    }
    
    func test_GetNewsDetail_CreateNewsDetailOjbect() {
        guard let data = createDataFromJSONFile(of: "newsDetail") else {
            XCTFail("no data from json file newsDetail.json")
            return
        }
        
        let engine = NetworkEngineMock(data: data, urlResponse: nil, error: nil)
        let client = NetworkClient(engine: engine)
        client.getNewsDetail(newId: 100) { (newsDetail, error) in
            guard let newsDetail = newsDetail else {
                XCTFail("newsDetail object is nil")
                return
            }
            
            XCTAssertEqual("title1", newsDetail.title)
            XCTAssertEqual("https://cdn.macrumors.com/article-new/2017/07/DEOsfxlXcAA7RYl.jpg-large-800x600.jpeg", newsDetail.imageUrlString)
            XCTAssertEqual("macrumors", newsDetail.source)
            XCTAssertEqual("content1", newsDetail.content)
            XCTAssertEqual("11/12/2016", newsDetail.dateString)
            XCTAssertNil(error)
        }
    }

}
