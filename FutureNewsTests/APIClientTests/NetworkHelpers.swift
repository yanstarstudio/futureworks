//
//  NetworkHelpers.swift
//  FutureNews
//
//  Created by Haiyan Ma on 20/08/2017.
//  Copyright © 2017 Haiyan Ma. All rights reserved.
//

import Foundation
import XCTest
@testable import FutureNews

extension NetworkClientTests {
    func httpResponse(isSuccess: Bool) -> HTTPURLResponse {
        let statusCode = isSuccess ? 200 : 500
        return HTTPURLResponse(url: urlRequest.url!, statusCode: statusCode,
                               httpVersion: nil, headerFields: nil)!
    }
    
    func getUrlComponents(for networkEngineMock: NetworkEngineMock) -> URLComponents? {
        guard let url = networkEngineMock.urlRequest?.url else {
            XCTFail()
            return nil
        }
        return URLComponents(url: url, resolvingAgainstBaseURL: true)
    }
}
