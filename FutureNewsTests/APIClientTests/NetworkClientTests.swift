//
//  NetworkClientTests.swift
//  FutureNewsTests
//
//  Created by Haiyan Ma on 19/08/2017.
//  Copyright © 2017 Haiyan Ma. All rights reserved.
//

import XCTest
@testable import FutureNews

class NetworkClientTests: XCTestCase {
    
    class NetworkEngineMock: NetworkEngine {
        
        private let data: Data?
        private let urlResponse: URLResponse?
        private let error: Error?
        
        var urlRequest: URLRequest?

        init(data: Data?, urlResponse: URLResponse?, error: Error?) {
            self.data = data
            self.urlResponse = urlResponse
            self.error = error
        }
        
        func performRequest(for urlRequest: URLRequest, completion: @escaping NetworkEngine.Handler) {
            self.urlRequest = urlRequest
            completion(data, urlResponse, error)
        }
    }
    
    override func setUp() {
        super.setUp()
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    var urlRequest: URLRequest {
        let url = URL(string: "https://www.google.com")!
        return URLRequest(url: url)
    }
    
    // --------------------------------
    // MARK:- Test Perform Request
    // --------------------------------
    func test_PerformRequestSucceeds_WhenThereIsData() {
        let data = "Hello world".data(using: .utf8)
        let engine = NetworkEngineMock(data: data, urlResponse: nil, error: nil)
        let client = NetworkClient(engine: engine)
        var result: Result<Data>?
        client.dataRequest(for: urlRequest) { result = $0 }
        
        guard let finalResult = result else { return }
        switch finalResult {
        case .success(let finalData):
            XCTAssertEqual(finalData, data)
        case .failure(let error):
            XCTAssertNil(error)
        }
    }
    
    func test_PerformResquestFails_WhenThereIsError() {
        let injectError = NSError(domain: "error", code: 1, userInfo: nil)
        let engine = NetworkEngineMock(data: nil, urlResponse: nil, error: injectError)
        let client = NetworkClient(engine: engine)
        var result: Result<Data>?
        client.dataRequest(for: urlRequest) { result = $0 }
        guard let finalResult = result else { return }
        switch finalResult {
        case .success(_):
            XCTFail()
        case .failure(let error):
            XCTAssertEqual(error.localizedDescription, injectError.localizedDescription)
        }
    }
    
    func test_PerformResquestFails_WhenHTTPResponseFails() {
        let httpesponse = httpResponse(isSuccess: false)
        let engine = NetworkEngineMock(data: nil, urlResponse: httpesponse, error: nil)
        let client = NetworkClient(engine: engine)
        var result: Result<Data>?
        client.dataRequest(for: urlRequest) { result = $0 }
        guard let finalResult = result else { return }
        switch finalResult {
        case .success(_):
            XCTFail()
        case .failure(let error):
           XCTAssertNotNil(error)
        }
    }
    
    // -----------------------------------
    // MARK:- Test Json parsing in "Get"
    // -----------------------------------
    func test_GetObjectSucceeds_WhenJSONParsingSucceeds() {
        let data = "{\"title\": \"albums\"}".data(using: .utf8)
        let engine = NetworkEngineMock(data: data, urlResponse: nil, error: nil)
        let client = NetworkClient(engine: engine)
        var result: Result<AnyObject>?
        client.getObject(with: "")  { result = $0 }
        
        guard let finalResult = result else { return }
        switch finalResult {
        case .success(let finalObject):
            guard let dict = finalObject as? [String : String] else {
                XCTFail()
                return
            }
            XCTAssertEqual(dict["title"], "albums")
        case .failure(let error):
            XCTAssertNil(error)
        }
    }

    func test_GetObjectFails_WhenJSONParsingFails() {
        
        let data = "".data(using: .utf8)
        let engine = NetworkEngineMock(data: data, urlResponse: nil, error: nil)
        let client = NetworkClient(engine: engine)
        var result: Result<AnyObject>?
        client.getObject(with: "")  { result = $0 }
        guard let finalResult = result else { return }
        
        switch finalResult {
        case .success(_):
           XCTFail("empty strong should not be able to be parsed as json object")
        case .failure(let error):
            XCTAssertNotNil(error)
        }
    }
}
