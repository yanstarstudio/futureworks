//
//  NewsFeedDataProvider.swift
//  FutureNews
//
//  Created by Haiyan Ma on 20/08/2017.
//  Copyright © 2017 Haiyan Ma. All rights reserved.
//

import XCTest
@testable import FutureNews

class NewsFeedDataProviderTests: XCTestCase {

    let sut = NewsDataProvider()
    var collectionView: UICollectionView!
    var controller: NewsFeedController!
    
    let newsList = [News(title: "title1", summary: "summary1", iconUrlString: "iconUrlString1", dateString: "date1", id: 100),
                    News(title: "title2", summary: "summary2", iconUrlString: "iconUrlString2", dateString: "date2", id: 200)]
    
    override func setUp() {
        super.setUp()
        
        sut.news = newsList
        controller = NewsFeedController(with: sut)
        _ = controller.view
        
        collectionView = controller.collectionView
        collectionView.dataSource = sut
        collectionView.delegate = sut
        collectionView.register(NewsCell.self, forCellWithReuseIdentifier: AppConfig.cellId)
    }
    
    func test_NumberOfSections_IsOne() {
        XCTAssertEqual(collectionView.numberOfSections, 1)
    }
    
    func test_NumberOfItemsInSection0() {
        XCTAssertEqual(collectionView.numberOfItems(inSection: 0), 2)
    }
    
    func test_CellForItem_ReturnsNewsCell() {
        let datasource = sut as UICollectionViewDataSource
        let cell = datasource.collectionView(collectionView, cellForItemAt: IndexPath(item: 0, section: 0))
        XCTAssertTrue(cell is NewsCell)
    }
}
