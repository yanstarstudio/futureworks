//
//  NewsFeedTests.swift
//  FutureNews
//
//  Created by Haiyan Ma on 20/08/2017.
//  Copyright © 2017 Haiyan Ma. All rights reserved.
//

import XCTest
@testable import FutureNews

class NewsFeedTests: XCTestCase {

    class MockNewsFeedController: NewsFeedController {
        var timesGetNewsCalled: Int = 0
        override func getNews() {
            super.getNews()
            timesGetNewsCalled += 1
        }
    }
    
    var sut: MockNewsFeedController!
    let dataProvider: NewsDataProvider = NewsDataProvider()
    
    override func setUp() {
        super.setUp()
        sut =  MockNewsFeedController(with: dataProvider)
        _ = sut.view // to trigger the call to viewDidLoad
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    func test_NewsDataProviderNotNil() {
        XCTAssertNotNil(sut.newsDataProvider)
    }
    
    func test_LoadingView_SetsCollectionViewDataSource() {
        XCTAssertTrue(sut.collectionView?.dataSource is NewsDataProvider)
    }
    
    func test_LoadingView_SetsCollectionViewDelegate() {
        XCTAssertTrue(sut.collectionView?.delegate is NewsDataProvider)
    }
    
    func test_FetchNewsCalledOnce() {
        XCTAssertEqual(sut.timesGetNewsCalled, 1)
    }
}
