//
//  NewsFeedViewModel.swift
//  FutureNews
//
//  Created by Haiyan Ma on 20/08/2017.
//  Copyright © 2017 Haiyan Ma. All rights reserved.
//

import XCTest
@testable import FutureNews

class NewsFeedViewModelTests: XCTestCase {
    
    struct TestDisplayable: NewsFeedDisplayable {
        let title = "News Title"
        let summary = "My Summary"
        let dateString = "02/05/2017"
        let iconUrlString = "icon url string"
        let id = 100
    }
    
    override func setUp() {
        super.setUp()
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    func test_NewsFeedDisplayable() {
        let displayable = TestDisplayable()
        XCTAssertEqual("News Title", displayable.title)
        XCTAssertEqual("My Summary", displayable.summary)
        XCTAssertEqual("02.05.2017", displayable.formattedDateString)
        XCTAssertEqual("icon url string", displayable.iconUrlString)
        XCTAssertEqual(100, displayable.id)
    }
}
