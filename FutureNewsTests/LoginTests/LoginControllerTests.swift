//
//  LoginControllerTests.swift
//  FutureNews
//
//  Created by Haiyan Ma on 21/08/2017.
//  Copyright © 2017 Haiyan Ma. All rights reserved.
//

import XCTest
@testable import FutureNews

class LoginControllerTests: XCTestCase {
    
    var sut: MockLoginController!
    
    class MockLoginController: LoginController {
        var timesGetBannerCalled: Int = 0
        override func getBannerImage() {
            super.getBannerImage()
            timesGetBannerCalled += 1
        }
    }
    
    override func setUp() {
        super.setUp()
        sut = MockLoginController()
        _ = sut.view // "view did load"
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    func test_TimesLoadBannerImageCalled() {
        XCTAssertEqual(sut.timesGetBannerCalled, 1)
    }
}
