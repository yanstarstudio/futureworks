//
//  ErrorPopup.swift
//  FutureNews
//
//  Created by Haiyan Ma on 21/08/2017.
//  Copyright © 2017 Haiyan Ma. All rights reserved.
//
import UIKit

class ErrorLauncher: NSObject {
    
    static let shared = ErrorLauncher()
    
    lazy var topBannerView: TopDropdownView = {
        let pbv = TopDropdownView(frame: .zero)
        return pbv
    }()
    
    func showBanner(with message: String) {
        topBannerView.errorLabel.text = message
        guard let keyWindow = UIApplication.shared.keyWindow else { return }
        
        if !keyWindow.subviews.contains(topBannerView) {
            let startFrame = CGRect(x: 0, y: -keyWindow.frame.height, width: keyWindow.frame.width, height: 70)
            topBannerView.frame = startFrame
            keyWindow.insertSubview(topBannerView, at: keyWindow.subviews.count)
        }
        
        // Add video Player view onto the main view
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.7, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            // define final frame
            self.topBannerView.frame.origin.y = 0
        }, completion: { (completed) in
            self.delay(seconds: 3, completion: {
                self.handleDismiss(delayInSecond: 0)
            })
        })
    }
    
    private func handleDismiss(delayInSecond: Double) {
        
        if let window = UIApplication.shared.keyWindow {
            UIView.animate(withDuration: 0.8, delay: delayInSecond, usingSpringWithDamping: 1.0, initialSpringVelocity: 0.6, options: .curveEaseOut, animations: {
                self.topBannerView.frame.origin.y = -window.frame.height
            }) { (completed) in
                self.topBannerView.removeFromSuperview()
            }
        }
    }
    
    // A delay function
    func delay(seconds: Double, completion:@escaping ()->()) {
        let popTime = DispatchTime.now() + Double(Int64( Double(NSEC_PER_SEC) * seconds )) / Double(NSEC_PER_SEC)
        
        DispatchQueue.main.asyncAfter(deadline: popTime) {
            completion()
        }
    }
}

class TopDropdownView: UIView {
    
    let errorLabel: RoundedLabel = {
        let label = RoundedLabel()
        label.numberOfLines = 0
        label.adjustsFontSizeToFitWidth = true
        label.minimumScaleFactor = 0.5
        label.textColor = .white
        label.font = UIFont.systemFont(ofSize: 16)
        label.text = "Verify your connection, this content is not available"
        label.textAlignment = .center
        return label
    }()
    
    func setupViews() {
        addSubview(errorLabel)
        backgroundColor = UIColor.themePink()
        errorLabel.anchorWithConstantsToTop(nil, left: leftAnchor, bottom: bottomAnchor, right: rightAnchor, topConstant: 0, leftConstant: 8, bottomConstant: 8, rightConstant: 8)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}




