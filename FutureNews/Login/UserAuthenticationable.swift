//
//  UserAuthenticationable.swift
//  FutureNews
//
//  Created by Haiyan Ma on 21/08/2017.
//  Copyright © 2017 Haiyan Ma. All rights reserved.
//

import UIKit

protocol UserAuthenticationable {
    func loginAction(with name: String)
    func logoutAction()
    func isLoggedIn() -> Bool
}

extension UserAuthenticationable where Self: UIViewController {
    
    var name: String? {
        let name = UserDefaults.standard.value(forKey: AppConfig.nameKey) as? String
        return name
    }
    
    func loginAction(with name: String) {
        UserDefaults.saveValueForKey(AppConfig.nameKey, value: name)
        dismiss(animated: true, completion: nil)
    }
    
    func logoutAction() {
        UserDefaults.removeValueForKey(AppConfig.nameKey)
        UserDefaults.removeValueForKey(AppConfig.favoriteKey)
        present(LoginController(), animated: true, completion: nil)
    }
    
    func isLoggedIn() -> Bool {
        guard let _ = UserDefaults.standard.value(forKey: AppConfig.nameKey) as? String else {
            return false
        }
        return true
    }
}
