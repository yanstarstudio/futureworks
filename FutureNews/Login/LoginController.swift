//
//  LoginController.swift
//  FutureNews
//
//  Created by Haiyan Ma on 21/08/2017.
//  Copyright © 2017 Haiyan Ma. All rights reserved.
//

import UIKit

class LoginController: UIViewController, UserAuthenticationable, KeyboardAvoidable {
    
    let bannerImageView: CustomImageView = {
        let iv = CustomImageView()
        iv.contentMode = .scaleAspectFill
        iv.layer.masksToBounds = true
        return iv
    }()
    
    lazy var nameTextField: UITextField = {
        let tf = UITextField()
        tf.placeholder = "Type your name"
        tf.borderStyle = .roundedRect
        tf.delegate = self
        return tf
    }()
    
    lazy var loginButton: RoundedButton = {
        let bn = RoundedButton(type: .custom)
        bn.setTitle("Log in", for: .normal)
        bn.titleLabel?.font = UIFont.systemFont(ofSize: 18)
        bn.backgroundColor = UIColor.themePink()
        bn.setTitleColor(UIColor.white, for: .normal)
        bn.addTarget(self, action: #selector(handleLogin), for: .touchUpInside)
        return bn
    }()
    
    let nameFieldContainer: UIView = {
        let view = UIView()
        return view
    }()
    
    var imageTopConstraint: NSLayoutConstraint?
    
    // MARK:- View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
        getBannerImage()
        uitestingId()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        addKeyboardObservers()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        removeKeyboardObservers()
        handleDismissKeyboard()
    }
    
    private func uitestingId() {
        view.accessibilityIdentifier = "loginView"
    }
    
    // MARK:- Actions
    func handleLogin() {
        guard let name = nameTextField.text, !name.characters.isEmpty else {
            self.nameTextField.shake()
            return
        }
        loginAction(with: name)
    }

    func handleDismissKeyboard() {
        view.endEditing(true)
    }
    
    // MARK:- Pull Data
    func getBannerImage() {
        guard let url = NetworkClient.shared.urlFromParameters([:], withPathExtension: Constants.Methods.loginBanner) else {
            return
        }
        bannerImageView.loadImageUsingUrlString(url.absoluteString)
    }
}

extension LoginController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
