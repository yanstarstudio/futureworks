//
//  KeyboardAvoidable.swift
//  FutureNews
//
//  Created by Haiyan Ma on 21/08/2017.
//  Copyright © 2017 Haiyan Ma. All rights reserved.
//

import UIKit

protocol KeyboardAvoidable {
    
    typealias KeyboardHeightDuration = (height: CGFloat, duration: Double)
    var imageTopConstraint: NSLayoutConstraint? { get }
    var loginButton: RoundedButton { get }
    func addKeyboardObservers()
    func removeKeyboardObservers()
    func animateConstraints(constant: CGFloat, duration: Double)
}

extension KeyboardAvoidable where Self: UIViewController {
    
    func addKeyboardObservers() {
        NotificationCenter.default.addObserver(forName: NSNotification.Name.UIKeyboardWillShow, object: nil, queue: nil, using: ({
            [weak self] notification in
            
            guard let stronSelf = self else { return }
            stronSelf.keyboardWillShow(notification: notification)
        }))
        
        NotificationCenter.default.addObserver(forName: NSNotification.Name.UIKeyboardWillHide, object: nil, queue: nil, using: ({
            [weak self] notification in
            
            guard let stronSelf = self else { return }
            stronSelf.keyboardWillHide(notification: notification)
        }))
    }
    
    func removeKeyboardObservers() {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    private func getKeyboardInfo(notification: Notification) -> KeyboardHeightDuration? {
        guard let userInfo = notification.userInfo else { return nil }
        
        guard let rect = (userInfo[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue else { return nil }
        guard let duration = (userInfo[UIKeyboardAnimationDurationUserInfoKey] as? Double) else { return nil }
        return (rect.height, duration)
    }
    
    private func keyboardWillShow(notification: Notification) {
        guard let info = getKeyboardInfo(notification: notification) else { return }
        animateConstraints(constant: info.height, duration: info.duration)
    }
    
    private func keyboardWillHide(notification: Notification) {
        guard let info = getKeyboardInfo(notification: notification) else { return }
        animateConstraints(constant: 0, duration: info.duration)
    }
    
    func animateConstraints(constant: CGFloat, duration: Double) {
        if constant == 0 {
            animateWithConstant(0, duration: duration)
            return
        }
        let loginButtonY = loginButton.frame.maxY
        let delta = constant - (UIScreen.main.bounds.height - loginButtonY)
        if delta > 0 {
            animateWithConstant(-(delta + 20), duration: duration)
        }
    }
    
    private func animateWithConstant(_ constant: CGFloat, duration: Double) {
        UIView.animate(withDuration: duration) {
            self.imageTopConstraint?.constant = constant
            self.view.layoutIfNeeded()
        }
    }
}
