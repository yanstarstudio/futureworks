//
//  Shakable.swift
//  FutureNews
//
//  Created by Haiyan Ma on 21/08/2017.
//  Copyright © 2017 Haiyan Ma. All rights reserved.
//

import UIKit

extension UITextField: Shakable {}

protocol Shakable {
    func shake()
}

extension Shakable where Self: UIView {
    func shake() {
        let animation = CABasicAnimation(keyPath: "position")
        animation.duration = 0.08
        animation.repeatCount = 5
        animation.autoreverses = true
        animation.fromValue = NSValue(cgPoint: CGPoint(x: self.center.x - 6.0, y: self.center.y))
        animation.toValue = NSValue(cgPoint: CGPoint(x: self.center.x + 6.0, y: self.center.y))
        layer.add(animation, forKey: "position")
    }
}
