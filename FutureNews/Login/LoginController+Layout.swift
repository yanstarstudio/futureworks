//
//  LoginController+Layout.swift
//  FutureNews
//
//  Created by Haiyan Ma on 21/08/2017.
//  Copyright © 2017 Haiyan Ma. All rights reserved.
//

import UIKit

extension LoginController {
    
    func setupViews() {
        view.backgroundColor = .white
        view.addSubViewList(bannerImageView, nameFieldContainer, loginButton)
        alignBannerImage()
        alignNameFieldContainer()
        alignLoginButton()
        view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleDismissKeyboard)))
    }
    
    private func alignBannerImage() {
        imageTopConstraint = bannerImageView.anchor(top: topLayoutGuide.bottomAnchor, left: view.leftAnchor, bottom: nil, right: view.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: UIScreen.main.bounds.height * 0.36).first
    }
    
    private func alignNameFieldContainer() {
        nameFieldContainer.anchorToTop(top: bannerImageView.bottomAnchor, left: view.leftAnchor, bottom: nil, right: view.rightAnchor)
        nameFieldContainer.proportionHeightToSuperView(0.24)
        alignNameTextField()
    }
    
    private func alignLoginButton() {
        loginButton.anchorCenterXToSuperview()
        loginButton.proportionWidthToSuperView(0.5)
        loginButton.heightAnchor.constraint(equalToConstant: 48).isActive = true
        loginButton.topAnchor.constraint(equalTo: nameFieldContainer.bottomAnchor).isActive = true
    }
    
    private func alignNameTextField() {
        nameFieldContainer.addSubview(nameTextField)
        nameTextField.anchorCenterSuperview()
        nameTextField.proportionWidthToSuperView(0.88)
        nameTextField.heightAnchor.constraint(equalToConstant: 40).isActive = true
    }
}

