//
//  NewsDetailController.swift
//  FutureNews
//
//  Created by Haiyan Ma on 20/08/2017.
//  Copyright © 2017 Haiyan Ma. All rights reserved.
//

import UIKit

class NewsDetailController: UICollectionViewController, Favoritable {
    
    var newsDetailProvider: NewsDetailProvider
    let favoriteButton = UIButton(frame: CGRect(x: 0, y: 0, width: 30, height: 30))
    
    // MARK:- Initiation
    init(with newsDetailProvider: NewsDetailProvider) {
        let layout = StretchyCollectionViewLayout()
        self.newsDetailProvider = newsDetailProvider
        super.init(collectionViewLayout: layout)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK:- View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupCollectionView()
        setCollectionViewProtocols()
        setFavoriteButton()
        getDetail()
        uitestingId()
    }
    
    // MARK:- Actions
    func handleFavorite() {
        let id = "\(newsDetailProvider.newsId)"
        
        guard var savedFavorite = savedFavorite() else {
            // mark as like and save locally
            createNewFavorite(for: id)
            setLikeButtonWithImageName(for: true)
            return
        }

        guard let favorite = savedFavorite[id] else {
            // mark as like and save locally
            insertNewFavorite(in: savedFavorite, for: id)
            setLikeButtonWithImageName(for: true)
            return
        }
        
        // toggle like and save locally
        toggleFavorite(in: savedFavorite, for: id, isFavorite: !favorite)
        setLikeButtonWithImageName(for: !favorite)
    }

    // MARK:- Setup Views
    private func setupCollectionView() {
        collectionView?.backgroundColor = .white
        collectionView?.alwaysBounceVertical = true
        collectionView?.showsVerticalScrollIndicator = true
        collectionView?.register(ContentCell.self, forCellWithReuseIdentifier: AppConfig.cellId)
        collectionView?.register(HeaderCell.self, forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: AppConfig.headerCellId)
    }
    
    private func setCollectionViewProtocols() {
        collectionView?.dataSource = newsDetailProvider
        collectionView?.delegate = newsDetailProvider
    }
    
    func setLikeButtonWithImageName(for isFavorite: Bool){
        let name = isFavorite ? "star_filled" : "star"
        var likeImage = UIImage(named: name)
        likeImage = likeImage?.withRenderingMode(.alwaysTemplate)
        favoriteButton.setBackgroundImage(likeImage, for: .normal)
        favoriteButton.tintColor = .white
    }
    
    private func setFavoriteButton() {
        let isFavorite = newsIDIsFavorite("\(newsDetailProvider.newsId)") ?? false
        setLikeButtonWithImageName(for: isFavorite)
        favoriteButton.addTarget(self, action: #selector(handleFavorite), for:.touchUpInside)
        let likeBarButtonItem = UIBarButtonItem(customView: favoriteButton)
        navigationItem.rightBarButtonItem = likeBarButtonItem
    }
    
    private func setTitle() {
        title = newsDetailProvider.newsDetail?.title
    }
    
    // MARK:- Setup Views
    private func uitestingId() {
        view.accessibilityIdentifier = "detailView"
    }
    
    // MARK:- Pull data from web
    func getDetail() {
        newsDetailProvider.fetchNewsDetail { (success) in
            if success {
                self.setTitle()
                self.collectionView?.reloadData()
            } else {
                 ErrorLauncher.shared.showBanner(with: "error loading News Detail... please try again later.")
            }
        }
    }
}
