//
//  FavoriteProtocol.swift
//  FutureNews
//
//  Created by Haiyan Ma on 21/08/2017.
//  Copyright © 2017 Haiyan Ma. All rights reserved.
//

import Foundation

protocol Favoritable {
    func createNewFavorite(for newsId: String)
    func insertNewFavorite(in favorites: [String: Bool], for newsId: String)
    func toggleFavorite(in favorites: [String: Bool], for newsId: String, isFavorite: Bool)
    func savedFavorite() -> [String : Bool]?
    func newsIDIsFavorite(_ newsId: String) -> Bool?
}

extension Favoritable {
    
    // retrieve saved favorites
    func savedFavorite() -> [String: Bool]? {
        guard let favorites = UserDefaults.standard.value(forKey: AppConfig.favoriteKey) as? [String: Bool] else {
            return nil
        }
        return favorites
    }
    
    func newsIDIsFavorite(_ newsId: String) -> Bool? {
        guard let favorites = savedFavorite() else {
            return nil
        }
        guard let favorite = favorites[newsId] else {
            return nil
        }
        return favorite
    }
    
    // save favorites
    func createNewFavorite(for newsId: String) {
        let favorite = [newsId: true]
        saveFavorite(favorite)
    }
    
    func insertNewFavorite(in favorites: [String: Bool], for newsId: String) {
        var updatedFavorites = favorites
        updatedFavorites[newsId] = true
        saveFavorite(updatedFavorites)
    }
    
    func toggleFavorite(in favorites: [String: Bool], for newsId: String, isFavorite: Bool) {
        var updatedFavorites = favorites
        updatedFavorites[newsId] = isFavorite
        saveFavorite(updatedFavorites)
    }
    
    private func saveFavorite(_ favorite: [String : Bool]) {
        UserDefaults.saveValueForKey(AppConfig.favoriteKey, value: favorite)
    }
}
