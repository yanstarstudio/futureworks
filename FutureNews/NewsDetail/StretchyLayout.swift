//
//  StretchyLayout.swift
//  FutureNews
//
//  Created by Haiyan Ma on 20/08/2017.
//  Copyright © 2017 Haiyan Ma. All rights reserved.
//

import UIKit

class StretchyCollectionViewLayout: UICollectionViewFlowLayout {
    
    var attributes: [UICollectionViewLayoutAttributes] = []
    
    override func prepare() {
        super.prepare()
        
        attributes = [] // Start with a fresh array of attributes
        
        // Can't do much without a collectionView.
        guard let collectionView = collectionView else {
            return
        }
        
        // Add all item in sections in attribute
        let numberOfSections = collectionView.numberOfSections
        for section in 0..<numberOfSections {
            let numberOfItems = collectionView.numberOfItems(inSection: section)
            for item in 0..<numberOfItems {
                let indexPath = IndexPath(item: item, section: section)
                if let attribute = layoutAttributesForItem(at: indexPath) {
                    attributes.append(attribute)
                }
            }
        }
        
        // Add header in attribute
        let headerIndexPath = IndexPath(item: 0, section: 0)
        if let headerAttribute = layoutAttributesForSupplementaryView(ofKind: UICollectionElementKindSectionHeader, at: headerIndexPath) {
            attributes.append(headerAttribute)
        }
    }
    
    // Telling our UICollectionViewLayout that it should be updating the layout while scrolling
    override func shouldInvalidateLayout(forBoundsChange newBounds: CGRect) -> Bool {
        return true
    }
    
    // As we scroll this method is called with the current visible rect plus a little padding.
    // We’ll use this constantly updating method to find our header and add to its height.
    override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
        
        let visibleAttributes = attributes.filter { attribute -> Bool in
            return rect.contains(attribute.frame) || rect.intersects(attribute.frame)
        }
        // Check for our Stretchy Header
        // We want to find a collectionHeader and stretch it while scrolling.
        // But first lets make sure we've scrolled far enough.
        let offset = collectionView?.contentOffset ?? CGPoint.zero
        let minY = -sectionInset.top
        
        if offset.y < minY {
            
            let extraOffset = fabs(offset.y - minY)
            
            // Find our collectionHeader and stretch it while scrolling.
            let stretchyHeader = visibleAttributes.filter { attribute -> Bool in
                return attribute.representedElementKind == UICollectionElementKindSectionHeader
                }.first
            
            if let collectionHeader = stretchyHeader {
                let headerSize = collectionHeader.frame.size
                collectionHeader.frame.size.height = max(minY, headerSize.height + extraOffset)
                collectionHeader.frame.origin.y = collectionHeader.frame.origin.y - extraOffset
            }
        }
        
        return attributes
    }
}
