//
//  HeaderCell.swift
//  FutureNews
//
//  Created by Haiyan Ma on 20/08/2017.
//  Copyright © 2017 Haiyan Ma. All rights reserved.
//

import UIKit

class HeaderCell: BaseCell {
    
    let imageView: CustomImageView = {
        let iv = CustomImageView()
        iv.contentMode = .scaleAspectFill
        iv.clipsToBounds = true
        return iv
    }()
    
    func configCellWith(_ newsDetailDisplayable: NewsDetailDisplayable) {
        imageView.loadImageUsingUrlString(newsDetailDisplayable.imageUrlString)
    }
    
     override func setupViews() {
        super.setupViews()
        addSubview(imageView)
        imageView.fillSuperview()
    }
}
