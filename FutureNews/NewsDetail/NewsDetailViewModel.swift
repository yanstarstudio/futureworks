//
//  NewsDetailViewModel.swift
//  FutureNews
//
//  Created by Haiyan Ma on 20/08/2017.
//  Copyright © 2017 Haiyan Ma. All rights reserved.
//

import UIKit

protocol NewsDetailDisplayable: AttributedStringHelpable {
    var title: String { get }
    var source: String { get }
    var dateString: String { get }
    var imageUrlString: String { get }
    var content: String { get }
}

extension NewsDetailDisplayable {
    var formattedDateString: String {
        return self.dateString.replacingOccurrences(of: "/", with: ".")
    }
    
    var attributedTitleString: NSAttributedString {
        let message = "\(source) - \(formattedDateString)"
        return combineTitleAndMessage(title: title,
                                         message: message,
                                         titleColor: .black,
                                         titleFont: UIFont.systemFont(ofSize: 16),
                                         messageColor: .lightGray,
                                         messageFont: UIFont.systemFont(ofSize: 14),
                                         jumpLine: true)
    }
}
