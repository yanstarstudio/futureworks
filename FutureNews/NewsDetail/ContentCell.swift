//
//  ContentCell.swift
//  FutureNews
//
//  Created by Haiyan Ma on 20/08/2017.
//  Copyright © 2017 Haiyan Ma. All rights reserved.
//

import UIKit

struct NewsDetailCellAlignment {
    static let spacing: CGFloat = 16.0
    static let defaultTitleHeight: CGFloat = 70.0
}

class ContentCell: BaseCell {
    
    let titleTextView: NonEditableTextView = {
        let tv = NonEditableTextView()
        return tv
    }()
    
    let contentTextView: NonEditableTextView = {
        let tv = NonEditableTextView()
        tv.font = UIFont.systemFont(ofSize: 16)
        tv.textColor = .lightGray
        return tv
    }()
    
    func configCellWith(_ newsDetail: NewsDetailDisplayable) {
        titleTextView.attributedText = newsDetail.attributedTitleString
        contentTextView.text = newsDetail.content
    }
    
    let spacing = NewsDetailCellAlignment.spacing

    override func setupViews() {
        super.setupViews()
        addSubViewList(titleTextView, contentTextView)
        alignTitleTextView()
        alignContentTextView()
    }
    
    private func alignTitleTextView() {
        titleTextView.anchorWithConstantsToTop(topAnchor, left: leftAnchor, bottom: nil, right: rightAnchor, topConstant: spacing, leftConstant: spacing, bottomConstant: 0, rightConstant: spacing)
        titleTextView.heightAnchor.constraint(equalToConstant: NewsDetailCellAlignment.defaultTitleHeight).isActive = true
    }
    
    private func alignContentTextView() {
        contentTextView.anchorWithConstantsToTop(titleTextView.bottomAnchor, left: leftAnchor, bottom: nil, right: rightAnchor, topConstant: 0, leftConstant: spacing, bottomConstant: 0, rightConstant: spacing)
    }
}
