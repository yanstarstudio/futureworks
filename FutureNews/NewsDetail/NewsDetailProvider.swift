//
//  NewsDetailProvider.swift
//  FutureNews
//
//  Created by Haiyan Ma on 20/08/2017.
//  Copyright © 2017 Haiyan Ma. All rights reserved.
//

import UIKit

class NewsDetailProvider: NSObject, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    init(newsId: Int) {
        self.newsId = newsId
        super.init()
    }
    
    var newsDetail: NewsDetail?
    var newsId: Int
    
    //MARK:- Fetch News Detail
    func fetchNewsDetail(completion: @escaping (_ isSuccessful: Bool) -> Void) {
        NetworkClient.shared.getNewsDetail(newId: newsId) { [weak self] (newsDetail, error) in
            guard error == nil, let strongSelf = self else {
                completion(false)
                return
            }
            strongSelf.newsDetail = newsDetail
            completion(true)
        }
    }
    
    //MARK:- Datasource
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: AppConfig.cellId, for: indexPath) as? ContentCell else {
            fatalError()
        }
        if let newsDetail = newsDetail {
            cell.configCellWith(newsDetail)
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        let header = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: AppConfig.headerCellId, for: indexPath) as! HeaderCell
        if let newsDetail = newsDetail {
            header.configCellWith(newsDetail)
        }
        return header
    }
    
    //MARK:- Delegateflowlayout
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        guard let newsDetail = newsDetail else {
            return CGSize(width: collectionView.frame.width, height: 0)
        }
        let newsContent = newsDetail.content
        let width = collectionView.frame.width - NewsDetailCellAlignment.spacing * 2
        let detailHeight = newsContent.estimateFrameForString(in: Int(width), height: 800, font: UIFont.systemFont(ofSize: 16)).height + 38
        return CGSize(width: collectionView.frame.width, height: detailHeight + NewsDetailCellAlignment.defaultTitleHeight)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize(width: collectionView.frame.width, height: collectionView.frame.height * 0.36)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
}
