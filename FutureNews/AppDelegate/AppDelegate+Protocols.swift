//
//  AppDelegate+Protocols.swift
//  FutureNews
//
//  Created by Haiyan Ma on 21/08/2017.
//  Copyright © 2017 Haiyan Ma. All rights reserved.
//

import Foundation
import UIKit

protocol AppBarStylable {
    func setNavBarStyle()
    func setStatusBarStyle(application: UIApplication)
}

extension AppBarStylable {

    func setNavBarStyle() {
        UINavigationBar.appearance().titleTextAttributes = [NSForegroundColorAttributeName: UIColor.white]
        UINavigationBar.appearance().tintColor = UIColor.white
        UINavigationBar.appearance().isTranslucent = false
        UINavigationBar.appearance().shadowImage = UIImage()
    }
    
    func setStatusBarStyle(application: UIApplication) {
        application.statusBarStyle = .lightContent
    }
}
