//
//  AppDelegate.swift
//  FutureNews
//
//  Created by Haiyan Ma on 19/08/2017.
//  Copyright © 2017 Haiyan Ma. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, AppBarStylable {

    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        setRootViewController()
        
        setNavBarStyle()
        setStatusBarStyle(application: application)
        
        uiTestingHelper()
        return true
    }

    private func setRootViewController() {
        window = UIWindow()
        window?.makeKeyAndVisible()
        window?.rootViewController = MainNavigationController()
    }
    
    // MARK:- UI Test Helpers
    private func uiTestingHelper() {
        if CommandLine.arguments.last == "--uitestingCreateUser" {
            createTester()
        } else if CommandLine.arguments.last == "--uitestingClean" {
            resetState()
        }
    }
    
    private func resetState() {
        UserDefaults.standard.removeObject(forKey: AppConfig.nameKey)
        UserDefaults.standard.removeObject(forKey: AppConfig.favoriteKey)
        UserDefaults.standard.synchronize()
        let defaultsName = Bundle.main.bundleIdentifier!
        UserDefaults.standard.removePersistentDomain(forName: defaultsName)
    }
    
    private func createTester() {
        UserDefaults.saveValueForKey(AppConfig.nameKey, value: "haiyan")
    }
}

