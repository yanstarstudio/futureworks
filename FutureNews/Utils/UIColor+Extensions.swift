//
//  UIColor+Extensions.swift
//  FutureNews
//
//  Created by Haiyan Ma on 21/08/2017.
//  Copyright © 2017 Haiyan Ma. All rights reserved.
//
import UIKit
import Foundation

extension UIColor {
    static func rgb(_ red: CGFloat, green: CGFloat, blue: CGFloat) -> UIColor {
        return UIColor(red: red/255, green: green/255, blue: blue/255, alpha: 1)
    }
    
    static func themePink() -> UIColor {
        return rgb(255, green: 47, blue: 121)
    }
    
    static func themeRed() -> UIColor {
        return rgb(255, green: 0, blue: 0)
    }
}
