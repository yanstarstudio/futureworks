//
//  CAGradientLayer.swift
//  FutureNews
//
//  Created by Haiyan Ma on 21/08/2017.
//  Copyright © 2017 Haiyan Ma. All rights reserved.
//

import UIKit
import QuartzCore

extension CAGradientLayer {
    class func gradientLayerForBounds(bounds: CGRect, colors: [CGColor]) -> CAGradientLayer {
        let layer = CAGradientLayer()
        layer.frame = bounds
        layer.startPoint = CGPoint(x: 0, y: 1)
        layer.endPoint = CGPoint(x: 0, y: 1)
        layer.colors = colors
        return layer
    }
}
