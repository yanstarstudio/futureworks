//
//  UserDefaults+Extensions.swift
//  FutureNews
//
//  Created by Haiyan Ma on 21/08/2017.
//  Copyright © 2017 Haiyan Ma. All rights reserved.
//

import Foundation

extension UserDefaults {
    
    static func saveValueForKey(_ key: String, value: Any) {
        standard.setValue(value, forKey: key)
        standard.synchronize()
    }

    static func removeValueForKey(_ key: String) {
        standard.removeObject(forKey: key)
        standard.synchronize()
    }
}
