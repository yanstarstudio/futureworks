//
//  String+Extensions.swift
//  FutureNews
//
//  Created by Haiyan Ma on 20/08/2017.
//  Copyright © 2017 Haiyan Ma. All rights reserved.
//

import UIKit

// MARK:- Helpers For Attributed String
protocol AttributedStringHelpable {
    func setParagraphStyleWith(lineSpacing: CGFloat, textAlignment: NSTextAlignment, attributedString: NSMutableAttributedString)
    func combineTitleAndMessage(title: String, message: String, titleColor: UIColor, titleFont: UIFont, messageColor: UIColor, messageFont: UIFont, jumpLine: Bool) -> NSMutableAttributedString
}

extension AttributedStringHelpable {
    // MARK: Set paragraph style
    // for NSMutableAttributedString
    func setParagraphStyleWith(lineSpacing: CGFloat, textAlignment: NSTextAlignment, attributedString: NSMutableAttributedString) {
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = lineSpacing
        paragraphStyle.alignment = textAlignment
        let range = NSMakeRange(0, attributedString.string.characters.count)
        attributedString.addAttribute(NSParagraphStyleAttributeName, value: paragraphStyle, range: range)
    }
    
    // MARK: Combine 2 strings
    // Combine 2 strings in one NSMutableAttributedString by setting their string content, font and color
    func combineTitleAndMessage(title: String, message: String, titleColor: UIColor, titleFont: UIFont, messageColor: UIColor, messageFont: UIFont, jumpLine: Bool) -> NSMutableAttributedString {
        
        let attributedText = NSMutableAttributedString(string: title, attributes: [NSFontAttributeName: titleFont, NSForegroundColorAttributeName: titleColor])
        var messageString = "\(message)"
        if jumpLine {
            messageString = title == "" ? message : "\n\(message)" // If there is no title, dont add line break
        }
        attributedText.append(NSAttributedString(string: messageString, attributes: [NSFontAttributeName: messageFont, NSForegroundColorAttributeName: messageColor]))
        return attributedText
    }
}

// MARK:- Estimate String Height
extension String {
    func estimateFrameForString(in width: Int, height: Int, font: UIFont) -> CGRect {
        
        let size = CGSize(width: width, height: height)
        let option = NSStringDrawingOptions.usesFontLeading.union(.usesLineFragmentOrigin)
        let rect = NSString(string: self).boundingRect(with: size,
                                                       options: option,
                                                       attributes: [NSFontAttributeName: font],
                                                       context: nil)
        return rect
    }
}


