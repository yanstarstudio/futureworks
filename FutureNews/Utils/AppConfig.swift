//
//  AppConfig.swift
//  FutureNews
//
//  Created by Haiyan Ma on 21/08/2017.
//  Copyright © 2017 Haiyan Ma. All rights reserved.
//

import Foundation

struct AppConfig {
    static let cellId = "cellId"
    static let headerCellId = "headerCellId"
    
    static let favoriteKey = "favoriteKey"
    static let nameKey = "nameKey"
}
