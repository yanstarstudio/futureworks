//
//  NewsFeedController.swift
//  FutureNews
//
//  Created by Haiyan Ma on 20/08/2017.
//  Copyright © 2017 Haiyan Ma. All rights reserved.
//

import UIKit

class NewsFeedController: UICollectionViewController, UserAuthenticationable {
    
    var newsDataProvider: NewsDataProvider
    var userName: String? {
        return name
    }
    
    // MARK:- Initiation
    init(with newsDataProvider: NewsDataProvider) {
        self.newsDataProvider = newsDataProvider
        let layout = UICollectionViewFlowLayout()
        super.init(collectionViewLayout: layout)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK:- View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupCollectionView()
        setCollectionViewProtocols()
        uitestingId()
        getNews()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setNavigationBar()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        title = ""
    }
    
    // MARK:- Action
    func handleLogout() {
        logoutAction()
    }
    
    // MARK:- Setup Views
    private func uitestingId() {
        view.accessibilityIdentifier = "newsfeedView"
    }
    
    private func setupCollectionView() {
        collectionView?.backgroundColor = .white
        collectionView?.alwaysBounceVertical = true
        collectionView?.showsVerticalScrollIndicator = true
        collectionView?.register(NewsCell.self, forCellWithReuseIdentifier: AppConfig.cellId)
    }
    
    private func setCollectionViewProtocols() {
        newsDataProvider.newsSelectionDelegate = self
        collectionView?.dataSource = newsDataProvider
        collectionView?.delegate = newsDataProvider
    }
    
    private func setNavigationBar() {
        setLogoutBarButton()
        setNaviagtionTitle()
    }
    
    private func setLogoutBarButton() {
        let logoutBarButtonItem = UIBarButtonItem(title: "Logout", style: .done, target: self, action: #selector(handleLogout))
        navigationItem.rightBarButtonItem = logoutBarButtonItem
    }
    
    private func setNaviagtionTitle() {
        title = (userName ?? "") + "' news feed"
    }
    
    // MARK:- Pull data from web
    func getNews() {
        newsDataProvider.fetchNews { (success) in
            if success {
                self.collectionView?.reloadData()
            } else {
                ErrorLauncher.shared.showBanner(with: "Error loading news... No news has been downloaded.")
            }
        }
    }
}

extension NewsFeedController: NewsSelectionDelegate {
    func presentDetail(with news: News) {
        let newsDetailProvider = NewsDetailProvider(newsId: news.id)
        let newsDetailController = NewsDetailController(with: newsDetailProvider)
        navigationController?.pushViewController(newsDetailController, animated: true)
    }
}
