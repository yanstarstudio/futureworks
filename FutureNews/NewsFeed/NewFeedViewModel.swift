//
//  NewFeedViewModel.swift
//  FutureNews
//
//  Created by Haiyan Ma on 20/08/2017.
//  Copyright © 2017 Haiyan Ma. All rights reserved.
//

import UIKit

protocol NewsFeedDisplayable {
    var title: String { get }
    var summary: String { get }
    var dateString: String { get }
    var iconUrlString: String { get }
    var id: Int { get }
    
}

extension NewsFeedDisplayable {
    var formattedDateString: String {
        return self.dateString.replacingOccurrences(of: "/", with: ".")
    }
}
