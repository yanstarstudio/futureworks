//
//  NewsDataProvider.swift
//  FutureNews
//
//  Created by Haiyan Ma on 20/08/2017.
//  Copyright © 2017 Haiyan Ma. All rights reserved.
//

import UIKit

protocol NewsSelectionDelegate: class {
    func presentDetail(with news: News)
}

class NewsDataProvider: NSObject, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    var news: [News]?
    weak var newsSelectionDelegate: NewsSelectionDelegate?

    //MARK:- Fetch News
    func fetchNews(completion: @escaping (_ isSuccessful: Bool) -> Void) {
        NetworkClient.shared.getNewsFeed(with: Constants.Methods.newsfeed) { (news, error) in
            guard error == nil else {
                completion(false)
                return
            }
            
            self.news = news
            completion(true)
        }
    }
    
    //MARK:- Datasource
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return news?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: AppConfig.cellId, for: indexPath) as? NewsCell else {
            fatalError()
        }
        if let news = news?[indexPath.item] {
            cell.configCell(with: news)
        }
        return cell
    }
    
    //MARK:- Flowlayout
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        guard let news = news?[indexPath.item] else {
            return CGSize(width: collectionView.frame.width, height: NewsCellConstants.defaultCellHeight)
        }
        let newsDetail = news.summary
        let width = collectionView.frame.width - 4 * NewsCellConstants.spacing - NewsCellConstants.imageWidth
        let detailHeight = newsDetail.estimateFrameForString(in: Int(width), height: 800, font: UIFont.systemFont(ofSize: 16)).height + 12
        let totalHeight = detailHeight + NewsCellConstants.titleHeight + 2 * NewsCellConstants.spacing
        let cellHeight = totalHeight > NewsCellConstants.defaultCellHeight ? totalHeight : NewsCellConstants.defaultCellHeight
        return CGSize(width: collectionView.frame.width, height: cellHeight)

    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    //MARK:- Delegate
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let news = news?[indexPath.item] else { return }
        newsSelectionDelegate?.presentDetail(with: news)
    }
}
