//
//  NewsCell.swift
//  FutureNews
//
//  Created by Haiyan Ma on 20/08/2017.
//  Copyright © 2017 Haiyan Ma. All rights reserved.
//

import UIKit

class NewsCell: BaseCell {
    
    let titleLabel: UILabel = {
        let lb = UILabel()
        lb.numberOfLines = 1
        lb.lineBreakMode = .byTruncatingTail
        return lb
    }()
    
    let summaryTextView: NonEditableTextView = {
        let tv = NonEditableTextView()
        tv.font = UIFont.systemFont(ofSize: 16)
        tv.textColor = UIColor.lightGray
        tv.isUserInteractionEnabled = false
        return tv
    }()
    
    let dateLabel: UILabel = {
        let lb = UILabel()
        lb.textAlignment = .center
        lb.font = UIFont.systemFont(ofSize: 14)
        return lb
    }()
    
    let iconImageView: CustomImageView = {
        let iv = CustomImageView()
        iv.contentMode = .scaleAspectFill
        iv.layer.cornerRadius = 8
        iv.layer.masksToBounds = true
        return iv
    }()
    
    let separationLine: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor(white: 0.7, alpha: 0.5)
        return view
    }()
    
    private let spacing = NewsCellConstants.spacing
    
    // MARK:- Config Cell
    func configCell(with newsDisplayable: NewsFeedDisplayable) {
        titleLabel.text = newsDisplayable.title
        summaryTextView.text = newsDisplayable.summary
        dateLabel.text = newsDisplayable.formattedDateString
        iconImageView.loadImageUsingUrlString(newsDisplayable.iconUrlString)
    }
    
    // MARK:- Setup Views
    override func setupViews() {
        super.setupViews()
        let leftContainerView = createImageAndDateContainerView()
        addSubViewList(leftContainerView, titleLabel, summaryTextView, separationLine)
        alignLeftContainer(leftContainerView)
        alignTitleAndSummaryView(to: leftContainerView)
        alignSeparationLine()
    }
    
    // MARK:- Setup Subviews
    private func alignLeftContainer(_ leftContainerView: UIView) {
        // Align left container view (image + date)
        _ = leftContainerView.anchor(top: topAnchor, left: leftAnchor, bottom: nil, right: nil, topConstant: spacing, leftConstant: spacing, bottomConstant: 0, rightConstant: 0, widthConstant: NewsCellConstants.imageWidth, heightConstant: NewsCellConstants.imageWidth + NewsCellConstants.dateLabelHeight)
    }
    
    private func alignTitleAndSummaryView(to leftContainerView: UIView) {
        // Align title label
        titleLabel.anchorWithConstantsToTop(topAnchor, left: leftContainerView.rightAnchor, bottom: nil, right: rightAnchor, topConstant: spacing, leftConstant: spacing * 2, bottomConstant: 0, rightConstant: spacing)
        titleLabel.heightAnchor.constraint(equalToConstant: NewsCellConstants.titleHeight).isActive = true
        
        // Align summary
        summaryTextView.anchorWithConstantsToTop(titleLabel.bottomAnchor, left: titleLabel.leftAnchor, bottom: bottomAnchor, right: titleLabel.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: spacing, rightConstant: 0)
    }
    
    private func alignSeparationLine() {
        separationLine.anchorToTop(top: nil, left: leftAnchor, bottom: bottomAnchor, right: rightAnchor)
        separationLine.heightAnchor.constraint(equalToConstant: 1.0).isActive = true
    }
    
    // MARK:- Helper
    private func createImageAndDateContainerView() -> UIView {
        let containerView = UIView()
        containerView.addSubViewList(iconImageView, dateLabel)
        
        iconImageView.anchorCenterXToSuperview()
        iconImageView.proportionHeightToSuperView(0.8)
        iconImageView.equalWidthToHeight()
        iconImageView.topAnchor.constraint(equalTo: containerView.topAnchor).isActive = true
        
        dateLabel.anchorToTop(top: iconImageView.bottomAnchor, left: iconImageView.leftAnchor, bottom: containerView.bottomAnchor, right: iconImageView.rightAnchor)
        return containerView
    }
}

