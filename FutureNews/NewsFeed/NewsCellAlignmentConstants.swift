//
//  NewsCellAlignmentConstants.swift
//  FutureNews
//
//  Created by Haiyan Ma on 20/08/2017.
//  Copyright © 2017 Haiyan Ma. All rights reserved.
//

import Foundation
import UIKit

struct NewsCellConstants {
    static let spacing: CGFloat = 8.0
    static let imageWidth = UIScreen.main.bounds.width * 0.3
    static let dateLabelHeight: CGFloat = 20.0
    static let titleHeight: CGFloat = 24.0
    static let defaultCellHeight = imageWidth + dateLabelHeight + 2 * spacing
}
