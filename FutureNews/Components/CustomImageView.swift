//
//  CustomImageView.swift
//  FutureNews
//
//  Created by Haiyan Ma on 20/08/2017.
//  Copyright © 2017 Haiyan Ma. All rights reserved.
//

import UIKit

let imageCache = NSCache<AnyObject, AnyObject>()

class CustomImageView: UIImageView {
    
    var imageUrlString: String?
    let loader = UIActivityIndicatorView(activityIndicatorStyle: .whiteLarge)
    
    func loadImageUsingUrlString(_ urlString: String) {
        
        loader.hidesWhenStopped = true
        loader.startAnimating()
        loader.color = UIColor.white
        self.addSubview(loader)

        self.backgroundColor = UIColor(white: 0.7, alpha: 0.5)
        
        loader.anchorCenterSuperview()
        loader.heightAnchor.constraint(equalToConstant: 24).isActive = true
        loader.widthAnchor.constraint(equalToConstant: 24).isActive = true
        
        imageUrlString = urlString
        
        self.image = nil
        guard let url = URL(string: urlString) else {
            self.loader.stopAnimating()
            return
        }
        
        // Load image from the cache if there is image in the cache
        if let imageFromCache = imageCache.object(forKey: urlString as AnyObject) as? UIImage{
            self.image = imageFromCache
            self.backgroundColor = .clear
            self.loader.stopAnimating()
            return
        }
        
        NetworkClient.shared.getImage(from: url, completion: { (image, error) in
            if error != nil {
                ErrorLauncher.shared.showBanner(with: "error dowloading image")
                print("loading image url error:", error ?? "something wrong")
                self.loader.stopAnimating()
                return
            }
            
            if self.imageUrlString == urlString {
                self.image = image
                self.backgroundColor = .clear
                self.loader.stopAnimating()
            }
            // ----------------------------------- //
            // Cache imageData
            imageCache.setObject(image ?? UIImage(), forKey: urlString as AnyObject)
        })
    }
}
