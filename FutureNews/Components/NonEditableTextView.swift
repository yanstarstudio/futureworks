//
//  NonEditableTextView.swift
//  FutureNews
//
//  Created by Haiyan Ma on 20/08/2017.
//  Copyright © 2017 Haiyan Ma. All rights reserved.
//

import UIKit

class NonEditableTextView: UITextView {
    
    override init(frame: CGRect, textContainer: NSTextContainer?) {
        super.init(frame: frame, textContainer: textContainer)
        defaultSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        defaultSetup()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        defaultSetup()
    }
    
    fileprivate func defaultSetup() {
        self.isEditable = false
        self.isSelectable = false
        self.isScrollEnabled = false
        self.backgroundColor = .clear
        self.textContainerInset = UIEdgeInsets.zero
        self.textContainer.lineFragmentPadding = 0
    }
}
