//
//  NetworkHelpers.swift
//  FutureNews
//
//  Created by Haiyan Ma on 19/08/2017.
//  Copyright © 2017 Haiyan Ma. All rights reserved.
//

import Foundation

protocol NetworkingHelpable {
    func performMainThreadUpdate(using closure: @escaping () -> Void)
}

extension NetworkingHelpable {
    
    // create a URL from parameters and path extensions
    func urlFromParameters(_ parameters: [String:AnyObject] = [:], withPathExtension: String? = nil) -> URL? {
        var components = URLComponents()
        components.scheme = Constants.ApiScheme
        components.host = Constants.ApiHost
        components.path = Constants.ApiPath + (withPathExtension ?? "")
        components.queryItems = [URLQueryItem]()
        
        for (key, value) in parameters {
            let queryItem = URLQueryItem(name: key, value: "\(value)")
            components.queryItems!.append(queryItem)
        }
        return components.url
    }
    
    func convertDataWithCompletionHandler(_ data: Data, completionHandlerForConvertData: (Result<AnyObject>) -> Void) {
        let dataToConvert = data
        do {
            let parsedResult = try JSONSerialization.jsonObject(with: dataToConvert, options: .allowFragments) as AnyObject
            completionHandlerForConvertData(Result.success(parsedResult))
        } catch {
            completionHandlerForConvertData(Result.failure(NetworkError.dataInvalid))
        }
    }
    
    func performMainThreadUpdate(using closure: @escaping () -> Void) {
        // If we are already on the main thread, execute the closure directly
        if Thread.isMainThread {
            closure()
        } else {
            DispatchQueue.main.async(execute: closure)
        }
    }
    
    // substitute the key for the value that is contained within the method name
    func substituteKeyInMethod(_ method: String, key: String, value: String) -> String? {
        if method.range(of: "{\(key)}") != nil {
            return method.replacingOccurrences(of: "{\(key)}", with: value)
        } else {
            return nil
        }
    }
}
