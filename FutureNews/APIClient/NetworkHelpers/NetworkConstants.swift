//
//  NetworkConstants.swift
//  FutureNews
//
//  Created by Haiyan Ma on 19/08/2017.
//  Copyright © 2017 Haiyan Ma. All rights reserved.
//

import Foundation

struct Constants {
    // MARK: URLs
    static let ApiScheme = "https"
    static let ApiHost = "s3.amazonaws.com"
    static let ApiPath = "/future-workshops"
    
    struct Methods {
        static let newsfeed = "/fw-coding-test.json" // get news feed list
        static let newsDetail = "/{id}.json" // get news detailed content
        static let loginBanner = "/fw-coding-login-image.jpg" // login banner image
    }
    
    struct JSONKeys {
        struct News {
            static let id = "id"
            static let title = "title"
            static let iconUrl = "icon_url"
            static let summary = "summary"
            static let content = "content"
            static let date = "date"
        }
        
        struct NewsDetail {
            static let id = "id"
            static let title = "title"
            static let imageUrl = "image_url"
            static let source = "source"
            static let content = "content"
            static let date = "date"
        }
    }
}
