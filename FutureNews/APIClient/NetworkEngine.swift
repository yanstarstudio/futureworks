//
//  NetworkEngine.swift
//  FutureNews
//
//  Created by Haiyan Ma on 19/08/2017.
//  Copyright © 2017 Haiyan Ma. All rights reserved.
//

import Foundation

enum HTTPMethod: String {
    case GET
    var stringValue: String {
        return self.rawValue
    }
}

enum Result<Value> {
    case success(Value)
    case failure(Error)
}

enum NetworkError: Error {
    case dataEmpty
    case dataInvalid
    case responseInvalid
    case urlInvalid
    case statusCode(Int)
}

protocol NetworkEngine {
    typealias Handler = (Data?, URLResponse?, Error?) -> Void
    func performRequest(for urlRequest: URLRequest, completion: @escaping Handler)
}

extension URLSession: NetworkEngine {
    typealias Handler = NetworkEngine.Handler
    func performRequest(for urlRequest: URLRequest, completion: @escaping NetworkEngine.Handler) {
        let task = dataTask(with: urlRequest, completionHandler: completion)
        task.resume()
    }
}
