//
//  NetworkClient.swift
//  FutureNews
//
//  Created by Haiyan Ma on 19/08/2017.
//  Copyright © 2017 Haiyan Ma. All rights reserved.
//

import Foundation
import UIKit

class NetworkClient: NetworkingHelpable {
    
    private let engine: NetworkEngine
    
    init(engine: NetworkEngine = URLSession.shared) {
        self.engine = engine
    }
    
    static let shared = NetworkClient()
    
    func dataRequest(for urlRequest: URLRequest, with completion: @escaping (Result<Data>) -> Void) {
        engine.performRequest(for: urlRequest) { (data, response, error) in
            if let error = error {
                completion(Result.failure(error))
                return
            }
            if let statusCode = (response as? HTTPURLResponse)?.statusCode, statusCode < 200 || statusCode > 299 {
                completion(Result.failure(NetworkError.statusCode(statusCode)))
                return
            }
            guard let data = data else {
                completion(Result.failure(NetworkError.dataEmpty))
                return
            }
            completion(Result.success(data))
        }
    }
    
    func getObject(with method: String, completion: @escaping (Result<AnyObject>) -> Void) {
        guard let url = urlFromParameters([:], withPathExtension: method) else {
            return
        }
        var request = NSMutableURLRequest(url: url) as URLRequest
        request.httpMethod = HTTPMethod.GET.stringValue
        dataRequest(for: request) { (data) in
            switch data {
            case .success(let finalData):
                self.convertDataWithCompletionHandler(finalData, completionHandlerForConvertData: completion)
            case .failure(let error):
                completion(Result.failure(error))
            }
        }
    }
    
    func getNewsFeed(with method: String, completion: @escaping (_ news: [News], _ error: Error?) -> Void) {
        
        getObject(with: method) { (result) in
            switch result {
            case .success(let dataObject):
                guard let dict = dataObject as? JSONDictionary, let newsDictArray = dict["articles"] as? [JSONDictionary]  else {
                    self.performMainThreadUpdate {
                        completion([], NetworkError.dataInvalid)
                    }
                    return
                }
                let news = newsDictArray.flatMap({ News(dict: $0) })
                self.performMainThreadUpdate { completion(news, nil)  }
            case .failure(let error):
                self.performMainThreadUpdate { completion([], error)  }
            }
        }
    }
    
    func getNewsDetail(newId: Int, completion: @escaping (_ newDetail: NewsDetail?, _ error: Error?) -> Void) {
        
        guard let detailMethod = substituteKeyInMethod(Constants.Methods.newsDetail, key: "id", value: "\(newId)") else {
            return
        }
        getObject(with: detailMethod) { (result) in
            switch result {
            case .success(let dataObject):
                guard let dict = dataObject as? [String: AnyObject] else {
                    self.performMainThreadUpdate {
                        completion(nil, NetworkError.dataInvalid)
                    }
                    return
                }
                let newsDetail = NewsDetail(dict: dict)
                self.performMainThreadUpdate { completion(newsDetail, nil)  }
            case .failure(let error):
                self.performMainThreadUpdate { completion(nil, error)  }
            }
        }
    }
    
    func getImage(from url: URL, completion: @escaping (_ image: UIImage?, _ error: Error?) -> Void) {
        dataRequest(for: URLRequest(url: url)) { (result) in
            switch result {
            case .success(let finalData):
                let image = UIImage(data: finalData)
                self.performMainThreadUpdate {
                    completion(image, nil)
                }
            case .failure(let error):
                self.performMainThreadUpdate {
                    completion(nil, error)
                }
            }
        }
    }
}
