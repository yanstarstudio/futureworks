//
//  MainNavigationController.swift
//  FutureNews
//
//  Created by Haiyan Ma on 21/08/2017.
//  Copyright © 2017 Haiyan Ma. All rights reserved.
//

import UIKit
import QuartzCore

class MainNavigationController: UINavigationController, UserAuthenticationable {
    
    // MARK:- View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setBaseViewController()
        presentLoginController()
        setNavigationBarWithGradientColor()
    }
    
    // MARK:- Config Views
    private func presentLoginController() {
        if !isLoggedIn() {
            perform(#selector(showLoginController), with: self, afterDelay: 0.01)
        }
    }
    
    private func setBaseViewController() {
        let newsDataProvider = NewsDataProvider()
        let newsFeedController = NewsFeedController(with: newsDataProvider)
        viewControllers = [newsFeedController]
    }
    
    private func setNavigationBarWithGradientColor() {
        let gradientImage = imageLayerForGradientBackground(navigationBar: navigationBar)
        navigationBar.setBackgroundImage(gradientImage, for: .default)
    }
    
    private func imageLayerForGradientBackground(navigationBar: UINavigationBar) -> UIImage {
        var updatedFrame = navigationBar.bounds
        // take into account the status bar
        updatedFrame.size.height += 20
        let layer = CAGradientLayer.gradientLayerForBounds(bounds: updatedFrame, colors: [UIColor.themePink().cgColor, UIColor.themeRed().cgColor])
        UIGraphicsBeginImageContext(layer.bounds.size)
        layer.render(in: UIGraphicsGetCurrentContext()!)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image!
    }
    
    // MARK:- Action
    func showLoginController() {
        let loginController = LoginController()
        present(loginController, animated: false, completion: { })
    }
}
