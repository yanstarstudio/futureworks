//
//  News.swift
//  FutureNews
//
//  Created by Haiyan Ma on 19/08/2017.
//  Copyright © 2017 Haiyan Ma. All rights reserved.
//

import Foundation

struct News: NewsFeedDisplayable {
    let title: String
    let summary: String
    let iconUrlString: String
    let dateString: String
    let id: Int
}

extension News: JSONDeserializable {
    init?(dict: JSONDictionary) {
        let news = Constants.JSONKeys.News.self
        guard let title = dict[news.title] as? String,
            let summary = dict[news.summary] as? String,
            let dateString = dict[news.date] as? String,
            let iconUrlString = dict[news.iconUrl] as? String,
            let id = dict[news.id] as? Int else {
                return nil
        }

        self.title = title
        self.summary = summary
        self.iconUrlString = iconUrlString
        self.dateString = dateString
        self.id = id
    }
}
