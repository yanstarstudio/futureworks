//
//  JSONDeserializable.swift
//  FutureNews
//
//  Created by Haiyan Ma on 20/08/2017.
//  Copyright © 2017 Haiyan Ma. All rights reserved.
//

import Foundation

typealias JSONDictionary = Dictionary<String, AnyObject>

protocol JSONDeserializable {
    init?(dict: JSONDictionary)
}
