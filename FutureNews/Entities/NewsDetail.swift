//
//  NewsDetail.swift
//  FutureNews
//
//  Created by Haiyan Ma on 19/08/2017.
//  Copyright © 2017 Haiyan Ma. All rights reserved.
//

import Foundation

struct NewsDetail: NewsDetailDisplayable {
    let title: String
    let source: String
    let dateString: String
    let content: String
    let imageUrlString: String
}

extension NewsDetail: JSONDeserializable {

    init?(dict: JSONDictionary) {
        let newsDetail = Constants.JSONKeys.NewsDetail.self
        guard let title = dict[newsDetail.title] as? String,
              let source = dict[newsDetail.source] as? String,
            let dateString = dict[newsDetail.date] as? String,
            let content = dict[newsDetail.content] as? String,
            let imageUrlString = dict[newsDetail.imageUrl] as? String else {
                return nil
        }
        self.title = title
        self.source = source
        self.dateString = dateString
        self.content = content
        self.imageUrlString = imageUrlString
    }
}
