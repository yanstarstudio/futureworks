//
//  NewsFeedUITests.swift
//  FutureNews
//
//  Created by Haiyan Ma on 21/08/2017.
//  Copyright © 2017 Haiyan Ma. All rights reserved.
//

import XCTest

class NewsFeedUITests: XCTestCase {
    
    var app: XCUIApplication!
    
    override func setUp() {
        super.setUp()
        
        continueAfterFailure = false
        
        app = XCUIApplication()
        app.launchArguments.removeAll()
        app.launchArguments.append("--uitestingCreateUser")
        app.launch()
    }
    
    override func tearDown() {
        super.tearDown()
        app.launchArguments.removeAll()
    }
    
    func test_TapLogout_GotoLoginView() {
        app.navigationBars.buttons["Logout"].tap()
        XCTAssertTrue(app.isDisplayingLogin)
    }
    
    func test_TapNews_GotoNewsDetail() {
        app.collectionViews.cells.children(matching: .other).element(boundBy: 0).tap()
        XCTAssertTrue(app.isDisplayingDetail)
    }
}

extension XCUIApplication {
    var isDisplayingLogin: Bool {
        return otherElements["loginView"].exists
    }
    
    var isDisplayingDetail: Bool {
        return otherElements["detailView"].exists
    }
}
