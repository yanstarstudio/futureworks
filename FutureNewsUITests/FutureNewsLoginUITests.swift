//
//  FutureNewsUITests.swift
//  FutureNewsUITests
//
//  Created by Haiyan Ma on 21/08/2017.
//  Copyright © 2017 Haiyan Ma. All rights reserved.
//

import XCTest

class FutureNewsLoginTests: XCTestCase {
    
    var app: XCUIApplication!

    override func setUp() {
        super.setUp()
        
        continueAfterFailure = false
        
        app = XCUIApplication()
        // We send a command line argument to our app,
        // to enable it to reset its state
        app.launchArguments.append("--uitestingClean")
        app.launch()
    }
    
    override func tearDown() {
        super.tearDown()
    }

    func test_TapLoginWithUserName_GotoNewsFeed() {
        let typeYourNameTextField = app.textFields["Type your name"]
        typeYourNameTextField.tap()
        typeYourNameTextField.typeText("H")
        typeYourNameTextField.typeText("a")
        app.buttons["Log in"].tap()
        XCTAssertTrue(app.isDisplayingNewsFeed)
    }
    
    func test_TapLoginWithoutUserName_Stay() {
        app.buttons["Log in"].tap()
        XCTAssertFalse(app.isDisplayingNewsFeed)
    }
}

extension XCUIApplication {
    var isDisplayingNewsFeed: Bool {
        return otherElements["newsfeedView"].exists
    }
}
